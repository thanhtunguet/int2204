package view;

import java.awt.*;

import javax.swing.*;

/**
 * This class extends and customizes the class JPanel Used for main panel of
 * main view
 * 
 * @author ubuntu
 */

public class VPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = -4950791982801012239L;

	private Image background;

	public VPanel(String backgroundURL, int width, int height) {
		setPreferredSize(new Dimension(width, height));
		ImageIcon bgIcon = new ImageIcon(getClass().getResource(backgroundURL));
		this.background = bgIcon.getImage();
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(background, 0, 0, null);
	}
}
