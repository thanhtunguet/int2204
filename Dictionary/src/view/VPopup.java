package view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import config.Config;
import model.Model;

/**
 * This class creates popup for add, update and delete word modules
 * 
 * @author ubuntu
 */

public class VPopup extends VFrame implements ActionListener, KeyListener {

	/**
	 *
	 */
	private static final long serialVersionUID = -2261555612888980837L;

	public static final int MODE_ADD = 0;

	public static final int MODE_UPDATE = 1;

	public static final int MODE_DELETE = 2;

	private View view;

	private Model model;

	public VPopup(int mode, Config config, Model model, View view) throws InterruptedException {

		super();

		addKeyListener(this);

		this.model = model;
		this.view = view;

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setSize(400, 150);
		setLocationRelativeTo(null);

		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.addKeyListener(this);

		textEnglish = new VTextField(15);
		textEnglish.addActionListener(this);
		textEnglish.addKeyListener(this);

		textVietnamese = new VTextField(15);
		textVietnamese.addActionListener(this);
		textVietnamese.addKeyListener(this);

		labelEnglish = new JLabel("English:_");
		labelEnglish.addKeyListener(this);
		labelVietnamese = new JLabel("Vietnamese:_");
		labelVietnamese.addKeyListener(this);

		panelEnglish = new JPanel(new GridBagLayout());
		panelEnglish.addKeyListener(this);

		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.insets = new Insets(1, 1, 1, 1);

		panelEnglish.add(Box.createHorizontalStrut(150), gbc);

		gbc.gridy = 1;
		panelEnglish.add(labelEnglish, gbc);

		gbc.gridx = 1;
		panelEnglish.add(textEnglish, gbc);

		panelVietnamese = new JPanel(new GridBagLayout());
		panelVietnamese.addKeyListener(this);

		gbc.gridx = 0;
		gbc.gridy = 0;

		panelVietnamese.add(Box.createHorizontalStrut(150), gbc);

		gbc.gridy = 1;
		panelVietnamese.add(labelVietnamese, gbc);

		gbc.gridx = 1;
		panelVietnamese.add(textVietnamese, gbc);

		labelStatus = new JLabel("Status");

		btnAdd = new JButton("Add");
		btnAdd.addActionListener(this);
		btnAdd.addKeyListener(this);

		btnUpdateEnglish = new JButton("Update En");
		btnUpdateEnglish.addActionListener(this);
		btnUpdateEnglish.addKeyListener(this);

		btnUpdateVietnamese = new JButton("Update Vi");
		btnUpdateVietnamese.addActionListener(this);
		btnUpdateVietnamese.addKeyListener(this);

		btnRemove = new JButton("Remove");
		btnRemove.addActionListener(this);
		btnRemove.addKeyListener(this);

		add(panel);

		panel.add(labelStatus);
		panel.add(panelEnglish);
		panel.add(panelVietnamese);

		switch (mode) {
		case MODE_ADD:
			setTitle(config.getString("lang-menu-edit-new"));
			newWord();
			break;
		case MODE_UPDATE:
			setTitle(config.getString("lang-menu-edit-update"));
			updateWord();
			break;
		case MODE_DELETE:
			setTitle(config.getString("lang-menu-edit-delete"));
			deleteWord();
			break;
		default:
			setTitle(config.getString("lang-null-popup-title"));
			break;
		}
		setVisible(true);
	}

	private JPanel panel, panelEnglish, panelVietnamese;

	private JTextField textEnglish, textVietnamese;

	private JLabel labelEnglish, labelVietnamese, labelStatus;

	private JButton btnAdd, btnUpdateEnglish, btnUpdateVietnamese, btnRemove;

	public void newWord() throws InterruptedException {
		panel.add(btnAdd);
		setVisible(true);
	}

	public void updateWord() {
		panel.add(btnUpdateEnglish);
		panel.add(btnUpdateVietnamese);
		setVisible(true);
	}

	public void deleteWord() {
		panel.add(btnRemove);
		setVisible(true);
	}

	public void showStatus(String status) {
		labelStatus.setText("Status: " + status);
		setVisible(true);
		// labelStatus.setText("Status");
		// setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object source = e.getSource();
		String english = textEnglish.getText();
		String vietnamese = textVietnamese.getText();
		if (source == btnAdd) {
			if (vietnamese.length() + english.length() == 0) {
				showStatus("Err! Cannot be empty.");
			} else if (model.wordExisted(english, vietnamese)) {
				showStatus("Err! Word existed.");
			} else {
				model.addWord(english, vietnamese);
				showStatus("Add " + english + " successfully");
			}
		} else if (source == btnUpdateEnglish) {
			showStatus(model.updateEnglish(english, vietnamese) ? "Update " + english + " successfully"
					: "Err! Cannot update");
		} else if (source == btnUpdateVietnamese) {
			showStatus(model.updateVietnamese(english, vietnamese) ? "Update " + vietnamese + " successfully"
					: "Err! Cannot update");
		} else if (source == btnRemove) {
			showStatus(model.deleteWord(english, vietnamese) ? "Delete " + english + " successfully"
					: "Err! Cannot delete");
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		super.dispose();
		view.reloadListWord();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			dispose();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
