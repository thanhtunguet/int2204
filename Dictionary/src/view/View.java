package view;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.swing.*;
import javax.swing.event.*;

import config.Config;
import controller.Controller;
import controller.Language;
import controller.WebURL;
import model.Word;
import model.Model;

/**
 * This class creates main view object for the application
 * 
 * @author ubuntu
 */

public class View extends VFrame implements ActionListener, KeyListener, DocumentListener {

	/**
	 *
	 */
	private static final long serialVersionUID = 7360212548484270185L;

	private Controller controller;

	private Model model;

	private Config config;

	private JMenuBar menuBar;

	private JMenu menuFile;

	private JMenu menuEdit;

	private JMenu menuAbout;

	private JMenuItem fileExit;

	private JMenuItem editNewWord;

	private JMenuItem editUpdateWord;

	private JMenuItem editDeleteWord;

	private JMenuItem aboutHelp;

	private JMenuItem aboutAbout;

	private VPanel panel = null;

	private JPanel panelSearch;

	private JPanel panelList;

	private JPanel panelDisplay;

	private VTextField textInput;

	private VButton btnSearch;

	private VButton btnMode;

	private VTextArea displayArea;

	private DefaultListModel<String> listModel;

	private JList<String> listWords;

	private JScrollPane listPane;

	public View(Config config, Controller controller, Model model) throws InterruptedException {
		super();
		this.controller = controller;
		this.model = model;
		this.config = config;
		addKeyListener(this);
		setSize(config.getInt("window-width"), config.getInt("window-height"));
		setLocationRelativeTo(null);
		setTitle(config.getString("app-name"));
		welcomeScreen();
		main();
	}

	/**
	 * Display a welcome screen
	 *
	 * @param config
	 * @throws InterruptedException
	 */
	public void welcomeScreen() throws InterruptedException {
		WelcomeImage welcomeImg = new WelcomeImage("/resource/coltech.jpg", this);
		add(welcomeImg);
		setVisible(true);
		TimeUnit.SECONDS.sleep(config.getLong("app-welcome-delay"));
		remove(welcomeImg);
	}

	/**
	 * Set menubar for main frame
	 */
	public void setMenuBar() {
		menuBar = new JMenuBar();
		menuBar.addKeyListener(this);

		menuFile = new JMenu(config.getString("lang-menu-file"));
		menuFile.addKeyListener(this);
		menuEdit = new JMenu(config.getString("lang-menu-edit"));
		menuEdit.addKeyListener(this);
		menuAbout = new JMenu(config.getString("lang-menu-about"));
		menuAbout.addKeyListener(this);

		fileExit = new JMenuItem(config.getString("lang-menu-file-exit"));
		fileExit.addActionListener(this);
		fileExit.addKeyListener(this);

		menuFile.add(fileExit);

		editNewWord = new JMenuItem(config.getString("lang-menu-edit-new"));
		editNewWord.addActionListener(this);
		editNewWord.addKeyListener(this);

		editUpdateWord = new JMenuItem(config.getString("lang-menu-edit-update"));
		editUpdateWord.addActionListener(this);
		editUpdateWord.addKeyListener(this);

		editDeleteWord = new JMenuItem(config.getString("lang-menu-edit-delete"));
		editDeleteWord.addActionListener(this);
		editDeleteWord.addKeyListener(this);

		menuEdit.add(editNewWord);
		menuEdit.addSeparator();
		menuEdit.add(editUpdateWord);
		menuEdit.addSeparator();
		menuEdit.add(editDeleteWord);

		aboutAbout = new JMenuItem(config.getString("lang-menu-about-me"));
		aboutAbout.setActionCommand("app-about");
		aboutAbout.addActionListener(this);
		aboutAbout.addKeyListener(this);

		aboutHelp = new JMenuItem(config.getString("lang-menu-about-help"));
		aboutHelp.setActionCommand("about-help");
		aboutHelp.addActionListener(this);
		aboutHelp.addKeyListener(this);

		menuAbout.add(aboutAbout);
		menuAbout.add(aboutHelp);

		menuBar.add(menuFile);
		menuBar.add(menuEdit);
		menuBar.add(menuAbout);

		setJMenuBar(menuBar);
	}

	/**
	 * Display main application
	 *
	 * @param config
	 */
	public void main() {
		setMenuBar();

		/**
		 * set main panel
		 */
		panel = new VPanel("/resource/background.jpg", config.getInt("window-width"), config.getInt("window-height"));
		panel.addKeyListener(this);

		textInput = new VTextField(config.getInt("text-input-cols"));
		textInput.getDocument().addDocumentListener(this);
		textInput.addKeyListener(this);

		btnSearch = new VButton(config.getString("lang-btn-translate"));
		switch (Language.LANG_DEST) {
		case Language.ENGLISH:
			btnMode = new VButton(config.getString("lang-mode-VN_EN"));
			break;
		default:
			btnMode = new VButton(config.getString("lang-mode-EN_VN"));
			break;
		}
		btnSearch.addActionListener(this);
		btnSearch.addKeyListener(this);

		btnMode.addActionListener(this);
		btnMode.addKeyListener(this);

		Dimension listDimesion = new Dimension(200, 480);

		listModel = new DefaultListModel<>();

		listWords = new JList<>(listModel);
		listWords.setSize(listDimesion);
		listWords.setCellRenderer(new TransparentListCellRenderer());
		listWords.setOpaque(false);
		listWords.addKeyListener(this);

		listPane = new JScrollPane(listWords);
		listPane.setOpaque(false);
		listPane.getViewport().setOpaque(false);
		listPane.addKeyListener(this);

		listWords.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				JList<?> source = (JList<?>) e.getSource();
				String value;
				try {
					value = (String) source.getSelectedValue();
				} catch (Exception e2) {
					// TODO: handle exception
					value = "";
				}

				ArrayList<Word> displayWords = model.getWords(value, Language.LANG_SRC);
				String displayText = "";
				for (int i = 0; i < displayWords.size(); i++) {
					if (i > 0) {
						displayText += "\n";
					}
					switch (Language.LANG_DEST) {
					case Language.ENGLISH:
						displayText += displayWords.get(i).toEnglish();
						break;

					default:
						displayText += displayWords.get(i).toVietnamese();
						break;
					}
					// displayText += displayWords.get(i).toString();
				}
				displayArea.setText(displayText);
				setVisible(true);
			}
		});

		panelList = new JPanel();
		panelList.setPreferredSize(listDimesion);
		panelList.setOpaque(false);
		panelList.setLayout(new BorderLayout());
		panelList.add(listPane, BorderLayout.CENTER);
		panelList.addKeyListener(this);

		panelDisplay = new JPanel();
		panelDisplay.setPreferredSize(new Dimension(560, 480));
		panelDisplay.setOpaque(false);
		panelDisplay.setBorder(BorderFactory.createTitledBorder(""));
		panelDisplay.addKeyListener(this);

		displayArea = new VTextArea(5, 42);
		displayArea.setLineWrap(true);
		displayArea.setWrapStyleWord(false);
		displayArea.setOpaque(false);
		displayArea.setBorder(BorderFactory.createTitledBorder("Meaning"));
		displayArea.addKeyListener(this);

		VTextArea guideArea = new VTextArea(20, 42);
		guideArea.setOpaque(false);
		guideArea.setEditable(false);
		guideArea.setBorder(BorderFactory.createTitledBorder("Guide"));
		guideArea.setText(config.getString("app-guide"));
		guideArea.setTabSize(4);
		guideArea.addKeyListener(this);

		panelDisplay.add(displayArea);
		panelDisplay.add(guideArea);

		reloadListWord();

		panelSearch = new JPanel();
		panelSearch.setOpaque(false);
		panelSearch.addKeyListener(this);

		panelSearch.add(btnMode);
		panelSearch.add(textInput, BorderLayout.NORTH);
		panelSearch.add(btnSearch, BorderLayout.NORTH);

		panel.add(panelSearch);
		panel.add(panelList);
		panel.add(panelDisplay);

		add(panel);
		setVisible(true);
	}

	public void exit() {
		dispose();
		System.out.println(config.getString("lang-exit"));
		System.exit(0);
	}

	public void reloadListWord() {
		listWords.clearSelection();
		String word;
		word = textInput.getText();
		ArrayList<String> words = model.findWords(word, Language.LANG_SRC);
		listModel.clear();
		for (int i = 0; i < words.size(); i++) {
			listModel.addElement(words.get(i));
		}
		displayArea.setText(null);
		listWords.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		repaint();
		setVisible(true);
	}

	public void changeTranslateMode() {
		listWords.clearSelection();
		Language.changeMode();
		switch (Language.LANG_SRC) {
		case Language.VIETNAMESE:
			btnMode.setText(config.getString("lang-mode-VN_EN"));
			break;

		default:
			btnMode.setText(config.getString("lang-mode-EN_VN"));
			break;
		}
		reloadListWord();
	}

	/**
	 * ActionEvent Handler
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object source = e.getSource();
		if (source == fileExit) {
			exit();
		} else if (source == aboutAbout) {
			WebURL.openFacebook(config);
		} else if (source == aboutHelp) {
			WebURL.openHelp(config);
		} else if (source == editNewWord) {
			try {
				controller.newWord();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if (source == editUpdateWord) {
			try {
				controller.updateWord();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if (source == editDeleteWord) {
			try {
				controller.deleteWord();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if (source == btnMode) {
			changeTranslateMode();
		} else if (source == btnSearch) {
			reloadListWord();
		}
	}

	/**
	 * KeyEvent Handler
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int keyCode = e.getKeyCode();
		if (e.isControlDown()) {
			switch (keyCode) {
			case KeyEvent.VK_W:
				exit();
				break;
			case KeyEvent.VK_N:
				try {
					controller.newWord();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				break;
			case KeyEvent.VK_E:
				try {
					controller.updateWord();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				break;
			case KeyEvent.VK_D:
				try {
					controller.deleteWord();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				break;
			case KeyEvent.VK_SPACE:
				changeTranslateMode();
				break;
			default:
				break;
			}
		}
	}

	/**
	 * DocumentEvent Handler
	 */
	@Override
	public void changedUpdate(DocumentEvent e) {
		// TODO Auto-generated method stub
		reloadListWord();
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		// TODO Auto-generated method stub
		reloadListWord();
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		// TODO Auto-generated method stub
		reloadListWord();
	}
}
