package view;

import java.awt.Color;

import javax.swing.JFrame;

/**
 * This class extends and cusomizes the class JFrame
 * 
 * @author ubuntu
 */

public class VFrame extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = -3628230098179637865L;

	public VFrame() {
		super();

		setDefaultCloseOperation(EXIT_ON_CLOSE);

		getContentPane().setBackground(Color.WHITE);

		setResizable(false);

		setFocusable(true);
	}
}
