package view;

import java.awt.Font;

/**
 * Customizes font for the application
 * 
 * @author ubuntu
 */

public class VFont extends Font {

	/**
	 *
	 */
	private static final long serialVersionUID = -2650936871482399072L;

	public VFont(String name, int style) {
		super(name, style, 14);
	}

	public VFont(String name) {
		super(name, Font.PLAIN, 14);
	}
}
