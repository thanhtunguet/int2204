package view;

import java.awt.Font;

import javax.swing.JTextField;

/**
 * This class extends and cusomizes the class JTextField
 * 
 * @author ubuntu
 */

public class VTextField extends JTextField {

	/**
	 *
	 */
	private static final long serialVersionUID = 8013800364165476314L;

	public VTextField(int cols) {
		super(cols);
		VFont font = new VFont("Open Sans", Font.BOLD);
		setFont(font);
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		String text = null;
		try {
			text = super.getText();
		} catch (Exception e) {
			// TODO: handle exception
			text = "";
		}
		return text;
	}
}
