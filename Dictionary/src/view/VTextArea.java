package view;

import java.awt.Font;

import javax.swing.JTextArea;

/**
 * This class extends and cusomizes the class JTextArea
 * 
 * @author ubuntu
 */

public class VTextArea extends JTextArea {

	/**
	 *
	 */
	private static final long serialVersionUID = -4888271187436213567L;

	public VTextArea(int rows, int cols) {
		super(rows, cols);
		VFont font = new VFont("Open Sans", Font.TRUETYPE_FONT);
		this.setFont(font);
	}
}
