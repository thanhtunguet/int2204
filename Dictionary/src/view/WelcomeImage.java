package view;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * This class is used for displaying welcome screen
 * 
 * @author ubuntu
 */

public class WelcomeImage extends Canvas {

	/**
	 *
	 */
	private static final long serialVersionUID = 7045458449952081249L;

	private int frameWidth;
	private int frameHeight;

	private Image image;

	public WelcomeImage(String resourceURL, JFrame frame) {
		frameWidth = frame.getWidth();
		frameHeight = frame.getHeight();
		image = (new ImageIcon(getClass().getResource(resourceURL))).getImage();
	}

	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		super.paint(g);
		g.drawImage(image, (frameWidth - image.getWidth(null)) / 2, (frameHeight - image.getHeight(null)) / 2, this);
	}
}
