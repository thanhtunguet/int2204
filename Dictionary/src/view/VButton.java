package view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;

/**
 * This class extends and cusomizes the class JButton
 * 
 * @author ubuntu
 */

public class VButton extends JButton {

	/**
	 *
	 */
	private static final long serialVersionUID = -5081885769121861572L;

	public VButton(String text) {
		super(text);
		setBackground(Color.GRAY);
		setForeground(Color.WHITE);
		setFocusPainted(false);
		VFont font = new VFont("Open Sans", Font.BOLD);
		setFont(font);
	}
}
