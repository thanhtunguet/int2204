package view;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

/**
 * Used for Create Transparent List Model
 * 
 * @author ubuntu
 */

public class TransparentListCellRenderer extends DefaultListCellRenderer {

	/**
	 *
	 */
	private static final long serialVersionUID = -8901220403636538417L;

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		// TODO Auto-generated method stub
		super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		setOpaque(isSelected);
		return this;
	}

}
