package controller;

/**
 * Define Language constants
 * 
 * @author ubuntu
 *
 */

public abstract class Language {

	public static final int ENGLISH = 0;

	public static final int VIETNAMESE = 1;

	/**
	 * source language and destination language
	 */
	public static int LANG_SRC = ENGLISH;

	public static int LANG_DEST = VIETNAMESE;

	public static void changeMode() {
		int tmp = LANG_SRC;
		LANG_SRC = LANG_DEST;
		LANG_DEST = tmp;
	}
}
