package controller;

import java.io.*;
import java.net.URISyntaxException;

import org.json.simple.parser.ParseException;

import config.Config;
import model.Model;
import view.*;

/**
 * Application main controller
 * 
 * @author ubuntu
 *
 */

public class Controller {

	public static void main(String[] args)
			throws IOException, ParseException, InterruptedException, URISyntaxException {
		new Controller();
	}

	private Config config;

	private Model model;

	private View view;

	public Controller() throws IOException, ParseException, InterruptedException, URISyntaxException {
		config = new Config();
		model = new Model(config.getString("app-database"));
		view = new View(config, this, model);
	}

	public void newWord() throws InterruptedException {
		new VPopup(VPopup.MODE_ADD, config, model, view);
	}

	public void updateWord() throws InterruptedException {
		new VPopup(VPopup.MODE_UPDATE, config, model, view);
	}

	public void deleteWord() throws InterruptedException {
		new VPopup(VPopup.MODE_DELETE, config, model, view);
	}
}
