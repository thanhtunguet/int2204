package controller;

import java.awt.Desktop;
import java.net.URI;

import config.Config;

/**
 * Open browser to visit links
 * 
 * @author ubuntu
 *
 */

public class WebURL {

	public static void openURL(String url) {
		if (Desktop.isDesktopSupported()) {
			Desktop desktop = Desktop.getDesktop();
			try {
				desktop.browse(new URI(url));
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		} else {
			Runtime runtime = Runtime.getRuntime();
			try {
				runtime.exec("xdg-open" + url);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	public static void openFacebook(Config config) {
		openURL(config.getString("app-author-facebook"));
	}

	public static void openHelp(Config config) {
		openURL(config.getString("app-author-help"));
	}
}
