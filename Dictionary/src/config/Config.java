package config;

import java.io.*;

import org.json.simple.*;
import org.json.simple.parser.*;

/**
 * Application's configuration
 * 
 * @author ubuntu
 *
 */

public class Config {

	protected JSONObject data;

	public Config() throws IOException, ParseException {
		JSONParser parser = new JSONParser();
		InputStreamReader in = new InputStreamReader(getClass().getResourceAsStream("/config/config.json"));
		data = (JSONObject) parser.parse(in);
	}

	public String getString(String key) {
		return (String) data.get(key);
	}

	public int getInt(String key) {
		return ((Long) data.get(key)).intValue();
	}

	public long getLong(String key) {
		return ((Long) data.get(key)).longValue();
	}

	public double getDouble(String key) {
		return ((Double) data.get(key)).doubleValue();
	}
}
