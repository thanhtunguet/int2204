package model;

import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import controller.Language;

/**
 * Application main model
 * 
 * @author ubuntu
 *
 */

public class Model extends Database {

	public Model(String dbFile) throws URISyntaxException {
		connect(dbFile);
	}

	public boolean addWord(String english, String vietnamese) {
		String query = "INSERT INTO `words`(`english`, `vietnamese`) VALUES ('" + english + "', '" + vietnamese + "');";
		return executeUpdate(query);
	}

	public ArrayList<String> findWords(String word, int lang) {
		ArrayList<String> words = new ArrayList<>();
		word = prepareQuery(word);
		String language;
		switch (lang) {
		case Language.ENGLISH:
			language = "english";
			break;

		default:
			language = "vietnamese";
			break;
		}
		String query;
		if (word.length() == 0) {
			query = "SELECT DISTINCT `" + language + "` FROM `words` ORDER BY `" + language + "` ASC;";
		} else {
			query = "SELECT DISTINCT `" + language + "` FROM `words` WHERE `" + language + "` LIKE '" + word
					+ "%' ORDER BY `" + language + "` ASC;";
		}
		try {
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(query);
			while (result.next()) {
				words.add(result.getString(language));
			}
			result.close();
			statement.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return words;
	}

	public ArrayList<Word> getWords(String word, int lang) {
		word = prepareQuery(word);
		ArrayList<Word> foundWords = new ArrayList<>();
		String language;
		switch (lang) {
		case Language.ENGLISH:
			language = "english";
			break;

		default:
			language = "vietnamese";
			break;
		}
		String query = "SELECT DISTINCT * FROM `words` WHERE `" + language + "` = '" + word + "';";
		try {
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(query);
			while (result.next()) {
				int id = result.getInt("id");
				String english = result.getString("english");
				String vietnamese = result.getString("vietnamese");
				foundWords.add(new Word(id, english, vietnamese));
			}
			result.close();
			statement.close();
			return foundWords;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getClass().getName() + ": " + e.getMessage());
			return foundWords;
		}
	}

	public boolean wordExisted(String english, String vietnamese) {
		english = prepareQuery(english);
		vietnamese = prepareQuery(vietnamese);
		return getWords(english, Language.ENGLISH).size() > 0 && getWords(vietnamese, Language.VIETNAMESE).size() > 0;
	}

	public boolean updateEnglish(String english, String vietnamese) {
		english = prepareQuery(english);
		vietnamese = prepareQuery(vietnamese);
		return executeUpdate("UPDATE `words` SET `english` = '" + english + "', `vietnamese` = '" + vietnamese
				+ "' WHERE `english` = '" + english + "';");
	}

	public boolean updateVietnamese(String english, String vietnamese) {
		english = prepareQuery(english);
		vietnamese = prepareQuery(vietnamese);
		return executeUpdate("UPDATE `words` SET `english` = '" + english + "', `vietnamese` = '" + vietnamese
				+ "' WHERE `vietnamese` = '" + vietnamese + "';");
	}

	public boolean deleteWord(String english, String vietnamese) {
		english = prepareQuery(english);
		vietnamese = prepareQuery(vietnamese);
		return executeUpdate(
				"DELETE FROM `words` WHERE `english` = '" + english + "' AND `vietnamese` = '" + vietnamese + "';");
	}
}
