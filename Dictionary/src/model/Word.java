package model;

import java.util.HashMap;
import controller.Language;

/**
 * Word structure
 * 
 * @author ubuntu
 *
 */

public class Word extends HashMap<String, String> {

	/**
	 *
	 */
	private static final long serialVersionUID = 6928146903513192751L;

	private int id;

	public Word(int id, String english, String vietnamese) {
		super();
		this.id = id;
		put("english", english);
		put("vietnamese", vietnamese);
	}

	public int id() {
		return this.id;
	}

	public String toEnglish() {
		return this.get("english");
	}

	public String toVietnamese() {
		return this.get("vietnamese");
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		if (Language.LANG_DEST == Language.ENGLISH) {
			return id + ". " + get("english") + ": " + get("vietnamese");
		} else {
			return id + ". " + get("vietnamese") + ": " + get("english");
		}
	}
}
