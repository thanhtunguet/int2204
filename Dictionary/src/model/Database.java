package model;

import java.io.File;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * Create, connect to database and execute queries
 *
 * @author ubuntu
 *
 */
public class Database {

	protected Connection connection;

	/**
	 * Establish connection
	 *
	 * @param dbFile
	 * @return
	 * @throws URISyntaxException
	 */
	public boolean connect(String dbFile) throws URISyntaxException {
		String path = getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
		path = (new File(path)).getParentFile().getAbsolutePath();
		dbFile = path + "/" + dbFile;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
	}

	/**
	 * Close connection
	 *
	 * @return
	 */
	public boolean close() {
		try {
			connection.close();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getClass().getName() + ": " + e.getMessage());
			return false;
		} finally {
			connection = null;
		}
	}

	/**
	 * Execute Insert, Update, Delete
	 *
	 * @param query
	 * @return boolean
	 */
	public boolean executeUpdate(String query) {
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
			statement.close();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	/*
	 * Add slashes to keyword of query
	 * Prevent SQL injection attacks
	 */
	public String prepareQuery(String query) {
		try {
			query = query.replaceAll("\\\\", "\\\\\\\\");
			query = query.replaceAll("\\n", "\\\\n");
			query = query.replaceAll("\\r", "\\\\r");
			query = query.replaceAll("\\00", "\\\\0");
			query = query.replaceAll("'", "\\\\'");
		} catch (Exception e) {
			// TODO: handle exception
		}
		return query;
	}
}
