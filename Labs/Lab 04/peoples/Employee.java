package peoples;


/**
 * This class defines an employee
 * @author Phạm Thanh Tùng
 *
 */

public class Employee extends Person {
	
	/**
	 * Salary number of this employee
	 */
	protected double salary;

	public double getSalary() {
		return salary;
	}
	
	/**
	 * Change employee to string
	 * format: {Name} ({date of birth}) {Salary}
	 */
	
	public String toString() {
		return this.name + " (" + this.birthday.toString() + ") " + this.salary;
	}
	
	/**
	 * Class constructor
	 * @param name		name of employee
	 * @param birthday	birthday of employee
	 * @param salary	salary of employee
	 */
	
	public Employee(String name, MyDate birthday, double salary) {
		super(name, birthday);
		if (salary >= 0) {
			this.salary = salary;
		}
	}
}
