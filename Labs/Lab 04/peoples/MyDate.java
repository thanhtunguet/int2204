package peoples;
import java.util.Scanner;

/**
 * Class MyDate of Lab 02
 * @author Phạm Thanh Tùng
 *
 */

public class MyDate {

	private int year;
	private int month;
	private int date;
	
	private Scanner scanner;

	public MyDate() {

		/**
		 * Default constructor: parameters set to Jan 01, 1970 You can change
		 * values to your own if you want :))
		 */

		date = 1;
		month = 1;
		year = 1970;
	}

	public MyDate(MyDate myDate) {

		/**
		 * Copy Constructor
		 */

		date = myDate.getDate();
		month = myDate.getMonth();
		year = myDate.getYear();
	}
	
	/**
	 * Constructor using parameters
	 */
	
	public MyDate(int date, int month, int year) {
		this.setDate(date);
		this.setMonth(month);
		this.setYear(year);
	}

	public void inputThenPrint() {

		/**
		 * Input data from keyboard then print to screen
		 */

		scanner = new Scanner(System.in);

		date = scanner.nextInt();
		month = scanner.nextInt();
		year = scanner.nextInt();

		System.out.format("This date is %d/%d/%d", date, month, year);
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}
	
	/**
	 * Change MyDate to a string
	 */
	
	public String toString() {
		return this.getDate() + "/" + this.getMonth() + "/" + this.getYear();
	}

}