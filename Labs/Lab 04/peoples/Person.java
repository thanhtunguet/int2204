package peoples;


/**
 * This class defines a person
 * @author Phạm Thanh Tùng
 *
 */

public class Person {

	/**
	 * The name of person
	 */
	protected String name;
	
	/**
	 * The birthday of person
	 */
	protected MyDate birthday;
	
	/**
	 * Class constructor with properties
	 * @param name		a string name of person
	 * @param birthday	a date value of person birthday
	 */
	
	public Person(String name, MyDate birthday) {
		this.name = name;
		this.birthday = birthday;
	}
	
	/**
	 * Name getter
	 * @return string name
	 */

	public String getName() {
		return name;
	}
	
	/**
	 * Change person to string:
	 * format: Name (date of birth)
	 */
	
	public String toString() {
		return this.name + " ("+ this.birthday.toString() + ")";
	}
}
