import peoples.Employee;
import peoples.MyDate;

/**
 * This class defines a manager
 * @author ommi
 *
 */

public class Manager extends Employee {
	
	/**
	 * Assistant of the manager
	 */
	
	private Employee assistant;
	
	/**
	 * Class constructor
	 */
	
	public Manager(String name, MyDate birthday, double salary) {
		super(name, birthday, salary);
	}
	
	/**
	 * Set assistant of manager
	 * @param assistant	the assistant
	 */

	public void setAssistant(Employee assistant) {
		this.assistant = assistant;
	}
	
	/**
	 * Print this manager to string
	 * format: {Name} ({Date of birth}) {Salary}. Assistant: {Assistant}
	 */
	
	public String toString() {
		return name + " (" + birthday.toString() + ") " + salary + ". Assistant: " + assistant.getName();
	}
}
