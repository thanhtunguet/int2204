import java.util.ArrayList;

import peoples.Employee;
import peoples.MyDate;
import peoples.Person;

public class PeopleTest {
	public static void main(String[] arg) {
		Employee newbie = new Employee("Newbie", new MyDate(10, 2, 1989), 1000000);
		Manager boss = new Manager("Boss", new MyDate(23, 2, 1979), 4000000);
		boss.setAssistant(newbie);
		Manager bigBoss = new Manager("Big Boss", new MyDate(3, 12, 1969), 10000000);
		bigBoss.setAssistant(boss);
		ArrayList<Person> p = new ArrayList<Person>();
		p.add(newbie);
		p.add(boss);
		p.add(bigBoss);
		for (int i = 0; i < p.size(); i++) {
			System.out.println(p.get(i).toString());
		}
		
	}
}
