
public class AnimalTest {
	
	public static void main(String[] arg) {
		Animal dog = new Animal("dog");
		dog.sayHello();
		Cow cow = new Cow("Legacy");
		cow.selfIntroduce();
	}
}
