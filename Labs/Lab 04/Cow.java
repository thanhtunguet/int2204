
/**
 * This class defines a cow
 * @author Phạm Thanh Tùng
 *
 */

public class Cow extends Animal {
	
	/**
	 * Class constructor
	 * Set name "cow"
	 */
	
	public Cow(String name) {
		this.setName(name);
	}
	
	public void sayHello() {
		System.out.println("Mooo...");
	}
}
