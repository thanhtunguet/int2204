
/**
 * This class defines an animal
 * @author Phạm Thanh Tùng
 *
 */

public class Animal {
	
	/**
	 * Name of the animal
	 */
	
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	/**
	 * Class constructor
	 * @param name init name of animal
	 */
	
	public Animal(String name) {
		if (name != null) {
			this.name = name;
		} else {
			this.name = this.getClass().getName();
		}
	}
	
	/**
	 * Constructor no param
	 */
	
	public Animal() {
		this.name = "No Name";
	}
	
	/**
	 * This method makes animal say a hello
	 */
	
	public void sayHello() {
		System.out.println("Hello, I'm a "+ name);
		System.out.println("Well,... I don't know what to say");
	}
	
	/**
	 * Animal introduces itself
	 */
	
	public void selfIntroduce() {
		System.out.println("Hello, My name is " + name);
		System.out.println("I'm a " + getClass().getName());
	}
}
