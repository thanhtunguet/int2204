
/**
 * This class defines a cat
 * @author Phạm Thanh Tùng
 *
 */

public class Cat extends Animal {
	
	/**
	 * Class constructor
	 * Set name "cat"
	 */
	
	public Cat(String name) {
		this.setName(name);
		
	}
	
	public void sayHello() {
		System.out.println("Meooo...");
	}
}
