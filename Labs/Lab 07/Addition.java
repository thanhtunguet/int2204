
public class Addition extends BinaryExpression {

	public Addition(Expression left, Expression right) {
		// TODO Auto-generated constructor stub
		super(left, right);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "(" + left().toString() + " + " + right().toString() + ")";
	}

	@Override
	public int evaluate() {
		// TODO Auto-generated method stub
		return left().evaluate() + right().evaluate();
	}
}
