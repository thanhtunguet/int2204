
public class Division extends BinaryExpression {
	
	public Division(Expression left, Expression right) {
		// TODO Auto-generated constructor stub
		super(left, right);
		if (right.evaluate() == 0) {
			throw new IllegalArgumentException("You've got a Field prize!!!");
		}
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "(" + left().toString() + " / " + right().toString() + ")";
	}

	@Override
	public int evaluate() {
		// TODO Auto-generated method stub
		return left().evaluate() / right().evaluate();
	}
}
