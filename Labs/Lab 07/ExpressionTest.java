
public class ExpressionTest {
	public static void main(String[] args) {
		Expression a = new Square(new Numeral(5));
		Expression b = new Subtraction(new Numeral(6), new Numeral(6));
		BinaryExpression aAb = new Division(a, b);
		System.out.println(aAb.toString() + " = " + aAb.evaluate());
	}
}
