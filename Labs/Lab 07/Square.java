
public class Square extends Expression {
	private Expression expression;

	public Square(Expression e) {
		// TODO Auto-generated constructor stub
		expression = e;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return expression.toString() + " ^ 2";
	}

	@Override
	public int evaluate() {
		// TODO Auto-generated method stub
		return expression.evaluate() * expression.evaluate();
	}
}
