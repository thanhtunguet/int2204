
public abstract class BinaryExpression extends Expression {
	private Expression left;
	private Expression right;
	
	public Expression left() {
		return this.left;
	}
	
	public Expression right() {
		return this.right;
	}
	
	public BinaryExpression(Expression left, Expression right) {
		// TODO Auto-generated constructor stub
		this.left = left;
		this.right = right;
	}
}
