
public class Numeral extends Expression {
	
	private int value;
	
	public Numeral() {
		value = 0;
	}
	
	public Numeral(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return Integer.toString(value);
	}

	@Override
	public int evaluate() {
		// TODO Auto-generated method stub
		return value;
	}
}
