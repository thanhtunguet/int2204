
public class SubString {

	public  int substr(String s1, String s2) {
		
		int len1 = s1.length(); // Length of s1
		int len2 = s2.length(); // Length of s2

		if (len1 > len2) {
			
			// s1 is larger than s2 so cannot be substr of s2
			
			return -1;
		}

		boolean FOUND = true;
		
		for (int i = 0; i < len2 - len1 + 1; i++) {
			
			for (int j = 0; j < len1; j++) {
				
				if (s1.charAt(j) != s2.charAt(j+i)) {
					
					FOUND = false;
					break;
				}
				
			}

			if (FOUND) {
				
				return i;
				
			} else {
				
				FOUND = true;
			}
		}
		
		return -1;
	}
	
}