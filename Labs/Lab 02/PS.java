
public class PS {

	/**
	 * Đây là kiểu dữ liệu đơn giản với tử số và mẫu số là số nguyên
	 */

	private int numerator; // Tử số
	private int denominator; // Mẫu số

	public PS(int nume, int deno) {

		// Rút gọn phân số nếu có thể

		int gcd = this.gCD(nume, deno);
		// Ước chung lớn nhất: Greatest Common Divisor
		nume /= gcd;
		deno /= gcd;

		this.numerator = nume;
		this.denominator = deno;
	}

	public PS(PS F) {

		this.numerator = F.getNumerator();
		this.denominator = F.getDenominator();
	}

	public int getNumerator() {

		return this.numerator;
	}

	public int getDenominator() {

		return this.denominator;
	}

	private int gCD(int a, int b) {

		// Phương thức này trả lại ước chung lớn nhất của 2 số nguyên: 
		// greatest common divisor
		// Phục vụ cho rút gọn phân số

		if (a == 0) {
			return (b != 0) ? b : 1;
		}

		if (b == 0) {
			return a;
		}

		int c;

		a = Math.abs(a);
		b = Math.abs(b);

		while (a != b) {
			c = Math.abs(a - b);
			a = Math.min(a, b);
			b = c;
		}

		return b;

	}

	public PS add(PS F) {

		// Phương thức cộng: add

		int nume = this.getNumerator() * F.getDenominator() + this.getDenominator() * F.getNumerator();
		int deno = this.getDenominator() * F.getDenominator();
		return new PS(nume, deno);
	}

	public PS sub(PS F) {

		// Phương thức trừ: subtract

		int nume = this.getNumerator() * F.getDenominator() - this.getDenominator() * F.getNumerator();
		int deno = this.getDenominator() * F.getDenominator();
		return new PS(nume, deno);
	}

	public PS mul(PS F) {

		// Phương thức nhân: multiple

		int nume = F.getNumerator() * this.getNumerator();
		int deno = F.getDenominator() * this.getDenominator();
		return new PS(nume, deno);
	}

	public PS div(PS F) {

		// Phương thức chia: divisive

		int nume = F.getDenominator() * this.getNumerator();
		int deno = F.getNumerator() * this.getDenominator();
		return new PS(nume, deno);
	}

}