
class NewString {

	/**
	 * New String type consists an original string of Java and concat-position
	 */

	public String value;
	public int concatPosition;

	public NewString(String str, int pos) {

		// Constructor

		this.value = str;
		this.concatPosition = pos;
	}
}

public class ConCat {

	public NewString concat(String s1, String s2) {

		return new NewString(s1 + s2, s1.length());
	}
}