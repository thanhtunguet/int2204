
public class CStack {
	
	protected SinglyLinkedList list;
	
	public CStack() {
		list = new SinglyLinkedList();
	}
	
	public Node push(String s) {
		return list.insertFirst(s);
	}
	
	public String pop() {
		return list.remove(list.first());
	}
}
