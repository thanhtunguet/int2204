
public class ListTest {
	
	public static void main(String[] args) {
		testInsert();
		testRemove();
	}
	
	public static void testInsert() {
		System.out.println("--- --- Call testInsert() --- ---");
		
		SinglyLinkedList sList = new SinglyLinkedList();
		
		Node ins = sList.insertFirst("This is the first node (ins)");
		
		sList.insertFirst("This is the first first node");
		sList.insertLast("This is the last node");
		sList.insertLast("This is the last last node");
		sList.insertAfter(ins, "Insert after ins");
		sList.insertBefore(ins, "Insert before ins");
		
		printList(sList);
		
		System.out.println("--- --- End testInsert() --- ---");
	}
	
	public static void testRemove() {
		System.out.println("--- --- Call testRemove() --- ---");
		
		SinglyLinkedList sList = new SinglyLinkedList();
		
		System.out.println(sList.remove(null));
		
		sList.insertFirst("This is the only one node of the list");
		
		System.out.println(sList.remove(sList.first()));
		
		printList(sList);
		
		sList.insertLast("This is my list");
		Node ins = sList.insertLast("This is the node will be deleted");
		sList.insertLast("End of my list");
		printList(sList);
		
		
		System.out.format("------------- Test Remove First\n");
		
		sList.remove(sList.first());
		
		printList(sList);
		
		System.out.format("------------- Test Remove Last\n");
		
		Node sLast = sList.last();
		sList.remove(sLast);
		
		printList(sList);
		
		System.out.format("-------------Test Remove Node\n");
		
		sList.remove(ins);
		
		printList(sList);
		
		Node notInTheList = new Node("This node isn't in the list");
		
		System.out.println	(sList.remove(notInTheList));
		
		printList(sList);
		
		
		System.out.println("--- --- End testRemove() --- ---");
	}
	
	public static void printList(SinglyLinkedList list) {
		System.out.format("// Print the list\n");
		Node run = list.first();
		int i = 0;
		while (run != null) {
			System.out.format("Node %d: %s\n", i, run.getData());
			i++;
			run = run.getNext();
		}
		if (i == 0) {
			System.out.format("List is empty\n");
		}
		System.out.format("// Finish printing the list\n");
	}
}
