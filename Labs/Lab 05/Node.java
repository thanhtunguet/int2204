
public class Node {
	
	/**
	 * String: data stored in Node
	 */
	
	protected String data;
	
	public void setData(String data) {
		this.data = data;
	}
	
	public String getData() {
		return this.data;
	}
	
	/**
	 * Node: the next Node in LinkedList
	 */
	
	private Node next;
	
	public void setNext(Node next) {
		this.next = next;
	}
	
	public Node getNext() {
		return this.next;
	}
	
	/**
	 * Constructors
	 * @param data will be stored in this node
	 * @param next pointer to the next node in a list
	 */
	
	public Node(String data, Node next) {
		this.setData(data);
		this.setNext(next);
	}
	
	public Node(String data) {
		this.setData(data);
		this.setNext(null);
	}
	
	public void print() {
		System.out.print(this.data);
	}
}
