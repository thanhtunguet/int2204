
public class CQueue {

	protected SinglyLinkedList list;
	
	public CQueue() {
		list = new SinglyLinkedList();
	}
	
	public Node enqueue(String s) {
		return list.insertLast(s);
	}
	
	public String dequeue() {
		return list.remove(list.first());
	}
}
