
public class SinglyLinkedList {
	
	/**
	 * The first node of the list
	 */
	
	protected Node head;
	
	/**
	 * The last node of the list
	 */
	
	protected Node tail;
	
	/**
	 * The list size, num of nodes on this list
	 */
	
	protected int size;
	
	public int size() {
		return this.size;
	}
	
	/**
	 * Constructor, create an empty list
	 */
	
	public SinglyLinkedList() {
		this.head = null;
		this.tail = null;
		this.size = 0;
	}
	
	/**
	 * This method returns the first node (head)
	 * @return Node
	 */
	
	public Node first() {
		return head;
	}
	
	/**
	 * This method returns the last node (tail)
	 * @return Node
	 */
	
	public Node last() {
		return tail;
	}
	
	/**
	 * This method finds and returns the next node of n
	 * if not found, returns null
	 * @param n the node will be used to find next
	 * @return Node
	 */
	
	public Node next(Node n) {
		if (n == null) {
			// Can't define n
			return null;
		}
		if (size <= 1) {
			// The next node doesn't exist
			// even n is the head of the list
			return null;
		}
		Node run = head;
		while (run != null && run != n) {
			run = run.getNext();
		}
		if (run == null) {
			// n isn't in the list
			return null;
		} else {
			return run.getNext();
		}
	}
	
	/**
	 * This method finds and returns the previous node of n
	 * if not found, returns null
	 * @param n
	 * @return
	 */
	
	public Node prev(Node n) {
		if (n == null) {
			return null;
		}
		if (size <= 1) {
			return null;
		}
		Node run = head;
		while (run.getNext() != null && run.getNext() != n) {
			run = run.getNext();
		}
		if (run == tail) {
			// n isn't in the list
			return null;
		} else {
			return run;
		}
	}
	
	/**
	 * This method removes a node from the list then returns data of that node
	 * @return String data of the node removed
	 */
	
	public String remove(Node n) {
		if (n == null) {
			return null;
		}
		
		String data = n.getData();
		
		if (n == head) {
			head = head.getNext();
			size--;
			return data;
		}
		
		if (n == tail) {
			tail = this.prev(tail);
			tail.setNext(null);
			size--;
			return data;
		}
		
		Node prevN = this.prev(n);
		if (prevN == null) {
			return data;
		}
		Node nextN = this.next(n);
		if (nextN == null) {
			return data;
		}
		
		prevN.setNext(nextN);
		size--;
		return data;
	}
	
	/**
	 * These methods insert a node in the list
	 * @param s data of new node
	 * @return reference to the new node
	 */
	
	public Node insertFirst(String s) {
		Node newNode = new Node(s, head);
		head = newNode;
		size++;
		if (size < 2) {
			tail = head;
		}
		return newNode;
	}
	
	public Node insertLast(String s) {
		Node newNode = new Node(s, null);
		if (size == 0) {
			head = newNode;
		} else {
			tail.setNext(newNode);
		}
		tail = newNode;
		size++;
		return newNode;
	}
	
	public Node insertAfter(Node n, String s) {
		// Note: this method doesn't check if n isn't in this list
		if (n == null) {
			insertLast(s);
			return tail;
		}
		Node newNode = new Node(s, n.getNext());
		n.setNext(newNode);
		size++;
		if (n == tail) {
			tail = newNode;
		}
		return newNode;
		
	}
	
	public Node insertBefore(Node n, String s) {
		if (n == null) {
			insertFirst(s);
			return head;
		}
		Node newNode = new Node(s, n);
		size++;
		if (n == head) {
			head = newNode;
		} else {
			Node prev = this.prev(n);
			prev.setNext(newNode);
		}
		return newNode;
	}
}
