
public class Phan2 {
	
	public static void main(String[] arg) {
		System.out.format("The greatest common divisor of 8 and 12 is: %d", Phan2.gCD(8, 12));
		System.out.format("\nThe 6th Fibonacci is %d", Phan2.fib(6));
	}
	
	public int gCD(int a, int b) {
		// Greatest Common Divisor
		if (a == 0) {
			return (b != 0) ? b : 1;
		}
		if (b == 0) {
			return a;
		}
		int c;
		a = Math.abs(a);
		b = Math.abs(b);
		while (a != b) {
			c = Math.abs(a - b);
			a = Math.min(a, b);
			b = c;
		}
		return b;

	}
	
	public static int fib(int N) {
		if (N == 0) {
			return 0;
		} else {
			if (N == 1) {
				return 1;
			} else {
				return fib(N-1) + fib(N-2);
			}
		}
	}
	
}
