
public class NickleNDime extends Account {
	
	public NickleNDime(double givenBalance) {
		super(givenBalance);
		// TODO Auto-generated constructor stub
	}

	private int withdrawCount = 0;
	
	public boolean withdraw(final double money) {
		if (super.withdraw(money)) {
			withdrawCount++;
			return true;
		} else {
			return false;
		}
	}
	
	public void endMonthCharge() {
		balance -= withdrawCount * 0.5;
	}
}
