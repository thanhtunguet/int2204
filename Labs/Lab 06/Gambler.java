
public class Gambler extends Account {

	public Gambler(double givenBalance) {
		super(givenBalance);
		// TODO Auto-generated constructor stub
	}

	public boolean withdraw(final double money) {
		if (super.withdraw(money)) {
			double pX2 = 0.51;
			double p = Math.random();
			if (p < pX2) {
				balance -= money;
			} else {
				balance += money;
			}
			return true;
		} else {
			return false;
		}
	}
}
