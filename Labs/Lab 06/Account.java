
public class Account {
	
//	The balance at current time
	
	protected double balance = 0;
	
//	Total transactions in current month
	protected int transactions = 0;
	
	/**
	 * Class constructor
	 * Create an account with given balance
	 */
	
	public Account(final double givenBalance) {
		balance = 0;
		if (givenBalance > 0) {
			balance += givenBalance;
		}
		transactions = 0;
	}
	
	public boolean deposit(final double money) {
		if (money > 0) {
			balance += money;
			transactions++;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean withdraw(final double money) {
		if (money > 0 && balance >= money) {
			balance -= money;
			transactions++;
			return true;
		} else {
			return false;
		}
	}
	
	public double endMonth() {
		endMonthCharge();
		System.out.format("Balance: %.2f, Total transactions: %d", balance, transactions);
		transactions = 0;
		return balance;
	}
	
	protected void endMonthCharge() {
		
	}
	
}
