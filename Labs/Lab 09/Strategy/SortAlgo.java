
public abstract class SortAlgo {
	
	public abstract void sort(NumList numList);
	
	public static void main(String[] args) {
		NumList numList = new NumList();
		numList.add(5);
		numList.add(6);
		numList.add(4);
		numList.add(3.5);
		numList.add(4.7);
		
		numList.setQuickSort();
		numList.setOrder();
		numList.print();
	}
}
