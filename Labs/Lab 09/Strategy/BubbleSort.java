
public class BubbleSort extends SortAlgo {

	@Override
	public void sort(NumList numList) {
		// TODO Auto-generated method stub
		double[] listOfNum = numList.getNumList();
		for (int i = 1; i < numList.size(); i++) {
			for (int j = 0; j < i; j++) {
				if (listOfNum[i] < listOfNum[j]) {
					double tmp = listOfNum[i];
					listOfNum[i] = listOfNum[j];
					listOfNum[j] = tmp;
				}
			}
		}
	}

}
