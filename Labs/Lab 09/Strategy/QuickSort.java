
public class QuickSort extends SortAlgo {

	@Override
	public void sort(NumList numList) {
		// TODO Auto-generated method stub
		double[] listOfNum = numList.getNumList();
		int size = numList.size();
		recursive(listOfNum, 0, size-1);
	}

	private void recursive(double[] list, int start, int stop) {
		int pivotIndex = partition(list, start, stop);
		System.out.println(pivotIndex + "");
		if (start < pivotIndex) {
			recursive(list, start, pivotIndex-1);
		}
//		if (pivotIndex < stop) {
//			recursive(list, pivotIndex+1, stop);
//		}
	}
	
	private void swap(double[] list, int i, int j) {
		list[i] += list[j];
		list[j] = list[i] - list[j];
		list[i] = list[i] - list[j];
	}
	
	private int partition(double[] list, int start, int stop) {
		int L = start;
		int R = stop;
		double pivot = list[(L + R) / 2];
		while (L <= R) {
			while (list[L] < pivot) {
				L++;
			}
			while (list[R] > pivot) {
				R--;
			}
			if (L <= R) {
				swap(list, L, R);
				L++;
				R--;
			}
		}
		return L;
	}
}
