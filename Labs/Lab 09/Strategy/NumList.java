
public class NumList {
	
	private SortAlgo sortAlgorithm;
	
	private double listOfNum[];
	
	private int length;	// The current size of the list
	
	private int LENGTH = 100; // Maximum number of the list
	
	public NumList() {
		listOfNum = new double[LENGTH];
		length = 0;
	}
	
	/**
	 * Get the size of the list
	 * @return
	 */
	
	public int size() {
		return length;
	}
	
	/**
	 * Get the list of numbers
	 * @return double
	 */
	
	public double[] getNumList() {
		return listOfNum;
	}
	
	/**
	 * Add a number into listOfNum
	 * @param number
	 */
	
	public void add(double number) {
		listOfNum[length] = number;
		length++;
	}
	
	/**
	 * Get the number at index
	 * @param index position of the number
	 * @return double
	 */
	
	public double get(int index) {
		return listOfNum[index];
	}
	
	/**
	 * Set the algorithm to BubbleSort()
	 */
	
	public void setBubbleSort() {
		sortAlgorithm = new BubbleSort();
	}
	
	/**
	 * Set the algorithm to QuickSort()
	 */
	
	public void setQuickSort() {
		sortAlgorithm = new QuickSort();
	}
	
	/**
	 * Do the sort operation
	 */
	
	public void setOrder() {
		sortAlgorithm.sort(this);
	}
	
	/**
	 * Print the list
	 */
	
	public void print() {
		for (int i = 0; i < size(); i++) {
			if (i > 0) {
				System.out.print(" ");
			}
			System.out.print(this.get(i));
		}
	}
}
