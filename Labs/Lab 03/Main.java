
public class Main {
		public static void main(String[] arg) {
			Stack stack = new Stack();
			
			// Check push()
			System.out.println(stack.pop());
			stack.push(5);
			stack.push(6);
			stack.push(7);
			
			// search() and numOfElements();
			
			System.out.format("Size = %d\n", stack.numOfElements());
			System.out.format("Search result: %d\n", stack.search(5));
			
			// Check display();
			
			System.out.print("Display items: ");
			stack.display();
			System.out.format("\nPop item: %d\n", stack.pop());
			
			// Check isEmpty();
			
			stack.pop();
			System.out.format("Stack is empty? %s\n", stack.isEmpty() ? "YES" : "NO");
			stack.pop();
			System.out.format("Stack is empty? %s\n", stack.isEmpty() ? "YES" : "NO");
		}
}
