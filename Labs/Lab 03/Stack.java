
public class Stack {
	
	private Node top;
	private int size;
	
/**
 * Getters and Setters auto-generated
 */
	
	public Node getTop() {
		return top;
	}

	public void setTop(Node top) {
		this.top = top;
	}

	public int getSize() {
		return size;
	}
	
/**
 * Constructors
 */
	
	public Stack() {
		this.size = 0;
		this.top = null;
	}
	
	/**
	 * Methods
	 */
	
	public void push(int item) {
		// push a node in the first position
		Node node = new Node(item, this.getTop());
		this.setTop(node);
		this.size++;
	}
	
	public int pop() {
		// Return the first item if not empty, else return -1
		if (size > 0) {
			Node popNode = this.top; // Node will be returned
			this.top = this.top.getNext();
			size--;
			return popNode.getItem();
		} else {
			return -1;
		}
	}
	
	public boolean isEmpty() {
		// If this stack is empty, return true, else return false
		return (size == 0);
	}
	
	public int numOfElements() {
		// Return this stack's size
		return this.size;
	}
	
	public int search(int item) {
		/**
		 * Search method
		 * @return int index of node found
		 * @else -1
		 */
		if (size > 0) {
			int i = 0;
			Node tmpNode = this.top;
			boolean found = false;
			while (tmpNode != null) {
				if (tmpNode.getItem() == item) {
					found = true;
				}
				if (found) {
					break;
				} else {
					i++;
					tmpNode = tmpNode.getNext();
				}
			}
			return found ? i : -1;
		} else {
			return -1;
		}
	}
	
	public void display() {
		// Display stack's elements
		Node tmp = this.top;
		int i = 0; // this number help separate items with space
		while (tmp != null) {
			if (i > 0) {
				System.out.print(" ");
			}
			System.out.format("%d", tmp.getItem());
			i++;
			tmp = tmp.getNext();
		}
	}
}
