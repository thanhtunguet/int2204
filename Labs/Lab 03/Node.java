
public class Node {
	
	private int item;
	private Node next;
	
	/**
	 * Constructors
	 */
	
	public Node(Node node) {
		this.next = node.getNext();
		this.item = node.getItem();
	}
	
	public Node(int item, Node next) {
		this.item = item;
		this.next = next;
	}

	/**
	 * 
	 * Getters and Setters
	 */
	public Node getNext() {
		return this.next;
	}
	
	public int getItem() {
		return this.item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public void setNext(Node next) {
		this.next = next;
	}
}
