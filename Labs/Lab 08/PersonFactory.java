
public class PersonFactory { 

	public enum PersonJob {
		TEACHER, STUDENT;
	}
	
	public static Person createPerson(PersonJob job) {
		Person p;
		switch (job) {
		case TEACHER:
			p = new Teacher();
			break;

		default:
			p = new Student();
			break;
		}
		return p;
	}
	
	public static Person createPerson(String name, String birthday, String className, PersonJob job) {
		Person p = null;
		switch (job) {
		case TEACHER:
			p = new Teacher(name, birthday, className);
			break;
		case STUDENT:
			p = new Student(name, birthday, className);
		}
		return p;
	}
}
