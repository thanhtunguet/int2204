
public class Clazz {
	
	private static Clazz instance = new Clazz();
	
	private String nameOfClass;
	
	private Student[] students;
	
	private Teacher teacher;
	
	public String getNameOfClass() {
		return nameOfClass;
	}

	public void setNameOfClass(String nameOfClass) {
		this.nameOfClass = nameOfClass;
	}

	public Student[] getStudents() {
		return students;
	}

	public void setStudents(Student[] students) {
		this.students = students;
	}
	
	public void addStudent(Student student, int index) {
		if (index == -1) {
			index = this.students.length;
		}
		this.students[index] = student;
	}
	
	public Student getStudent(int index) {
		return this.students[index];
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Person teacher) {
		this.teacher = (Teacher) teacher;
	}

	private Clazz() {
		nameOfClass = null;
		teacher = null;
	}
	
	public static Clazz getInstance() {
		return instance;
	}
	
	public static void main(String[] args) {
		Clazz clazz = Clazz.getInstance();
		Person teacher = (Teacher) PersonFactory.createPerson(PersonFactory.PersonJob.TEACHER);
		clazz.setTeacher(teacher);
		Student[] students = new Student[10];
		for (int i = 0; i < 10; i++) {
			students[i] = (Student) PersonFactory.createPerson(PersonFactory.PersonJob.STUDENT);
		}
		clazz.setStudents(students);
		clazz.getTeacher().work();
		for (int i = 0; i < 10; i ++) {
			clazz.getStudent(i).work();
		}
	}
}
