
public abstract class Person {
	
	public static final String LEARNING = "Learning";
	
	public static final String TEACHING = "Teaching";
	
	public static final String UNKNOWN_WORK = "Unknown work";
	
	private String fullname;

	private String birthday;

	private String className;

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public abstract void work();
	
	public Person() {
		
	}
	
	public Person(String name, String birthday, String className) {
		this.setFullname(name);
		this.setBirthday(birthday);
		this.setClassName(className);
	}
}
